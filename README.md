# B2G long exercise: $b^\ast \to tW$

The overall goal of long exercises is to offer everyone a high-level exposure to the different components of a physics analysis. In practice, carrying out an analysis takes months and often past a year; we hope to cover the key points of the process and won't have too much time to do so. To help you through the material, we will be providing some useful code for your use. We strongly suggest that you focus on understanding the conceptual flow of the analysis and ask as many questions as you would like. We also want to encourage you to get creative and challenge yourself! If there are any parts of the exercise that you feel you can tackle by writing your own scripts, or optimizing ours, please do! We are all collaborators and can learn a lot from each other.

This search looks for a heavy resonance that decays into a top quark and a W boson. In this exercise we will focus on the hadronic final state only. This means that the subsequent decay products of the top quark and the W boson are fully hadronic, i.e. $t \to bW(q\bar{q})$ and $W \to q\bar{q}$. While the search for this final state is generic, we use an excited quark, $b^\ast$, as a benchmark model for the signal.

An important aspect of this analysis is the reconstruction of the final state. Since the resonance is heavy, its decay products will have large Lorentz boost and will be collimated into a single, large-radius jet with a distinct substructure. Therefore, this analysis uses jet-substructure techniques to identify the top and W jets.

The second aspect of this analysis is the estimation of the background. The background is dominated by jets produced through the strong interaction, referred to as quantum chromodynamics (QCD) multijet production, or "QCD background" and is estimated with a data-driven method. This means that we use data in a signal-depleted region to infer how much QCD background we have instead of using Monte Carlo simulation to predict the QCD contribution. This is done for QCD backgrounds in particular because they are difficult to model in simulation and so a more robust estimate of the background can be obtained by sampling from the data. 

# Set up

The instructions in the `Event_Selection` and `Background_Estimate` directories assume that you will clone this repository to your `~/public/` directory on LXPLUS. To do so, please run the following commands:

```
ssh -Y USERNAME@lxplus8.cern.ch
cd ~/public/
git clone ssh://git@gitlab.cern.ch:7999/cmsdas-cern-2024/long-ex-b2g-bstar.git
```

You can then use the `README.md` files in the two exercises subdirectories to install and set up the necessary software packages for the event selection and background estimate exercises. 