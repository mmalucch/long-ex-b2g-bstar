# Long exercise part 1: Event Selection
The code in this directory is used for the first part of the B2G long exercise - the event selection.

# Getting started (Alma Linux 8)

<details>
<summary>First-time installation</summary>
<br>
The following instructions assume that you have already cloned this main exercise repository following the `README` in the base directory (e.g., under `~/public/long-ex-b2g-bstar/`) and that you have added [SSH keys](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account) to your github account.

```
ssh -Y USERNAME@lxplus8.cern.ch
cd ~/public/long-ex-b2g-bstar/Event_Selection
cmsrel CMSSW_12_3_5
cd CMSSW_12_3_5
cmsenv
cd ..
python3 -m virtualenv timber-env
git clone git@github.com:JHU-Tools/TIMBER.git
cd TIMBER/
mkdir bin && cd bin
git clone git@github.com:fmtlib/fmt.git
cd ../../
```

Boost library path (the boost version as well!) may change depending on the CMSSW version so this may need to be modified by hand. This version works for both CMSSW versions used for lxplus8 and lxplus9. If one does not wish to use CMSSW, boost libraries will have to be installed (and added to the MakeFile). Copy the whole multi-line string to the environment activation script by pasting the following block in the shell:

```
cat <<EOT >> timber-env/bin/activate

export BOOSTPATH=/cvmfs/cms.cern.ch/el8_amd64_gcc10/external/boost/1.78.0-0d68c45b1e2660f9d21f29f6d0dbe0a0/lib
if grep -q '\${BOOSTPATH}' <<< '\${LD_LIBRARY_PATH}'
then
  echo 'BOOSTPATH already on LD_LIBRARY_PATH'
else
  export LD_LIBRARY_PATH=\${LD_LIBRARY_PATH}:\${BOOSTPATH}
  echo 'BOOSTPATH added to PATH'
fi
EOT
```

Finally, activate the python3 environment, set a proper `LD_LIBRARY_PATH` for boost libraries and build the TIMBER binaries: 

```
source timber-env/bin/activate
cd TIMBER
source setup.sh
```

You can test that the TIMBER installation is working by running the following in your shell:

```
python -c 'import TIMBER.Analyzer'
```

If all went well, the command should be executed with no output.

</details>

<details>
<summary>What to do every time you log in to LXPLUS, after installation</summary>
<br>

Once you have set up TIMBER, you only need to source the CMSSW environment and load the python virtual environment to use it:

```
ssh -Y USERNAME@lxplus8.cern.ch
cd ~/public/long-ex-b2g-bstar/Event_Selection
cd CMSSW_12_3_5
cmsenv
cd ..
source timber-env/bin/activate
```

You will need to perform this step every time you log on to the LXPLUS cluster.

</details>

# Event selection in TIMBER

<details>
<summary>Some background information on TIMBER</summary>
<br>

[TIMBER](https://github.com/JHU-Tools/TIMBER) (Tree Interface for Making Binned Events with RDataFrame) is an easy-to-use and fast python analysis framework used to process CMS data sets. Default arguments assume the use of the CMS [NanoAOD](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD) format but any ROOT TTree can be processed.

TIMBER's processing speed comes from the use of ROOT's [RDataFrame](https://root.cern/doc/master/classROOT_1_1RDataFrame.html). RDataFrame offers "multi-threading and other low-level optimizations" which make analysis level processing faster and more efficient than traditional python for loops. However, RDataFrame derives its speed from its C++ back-end and while an RDataFrame object can be instantiated and manipulated in python, any actions on it are written in C++ (even if you're using python).

In an RDataFrame each row of the table is a separate event and each column is a different variable in the event (a branch, in TTree terms). Columns can be single values or vectors (specifically a [`ROOT::VecOps:RVec`](https://root.cern.ch/doc/master/classROOT_1_1VecOps_1_1RVec.html)). Since each row is an event, vectors are necessary for the case of multiple of the same physics object in an event - for example, multiple electrons. **NOTE**: NanoAOD orders these vectors by $p_{T}$ of the objects. So if you'd like the $\eta$ of the leading electron in an event, it is stored as `Electron_eta[0]`.

This can make accessing values tricky! For example, if there's only one electron in an event and the analyzer asks for `Electron_eta[1]`, the computer will return a segfault because the analyzer tried to index the `Electron_eta` vector out of bounds. These are the types of problems that TIMBER attempts to solve (even if it's just by users sharing their experiences).

We strongly encourage you to get started with TIMBER by performing or reviewing the standalone exercises included in TIMBER [here](https://github.com/JHU-Tools/TIMBER/tree/master/examples). As well, you can run TIMBER standalone in a Docker container by following the instructions in [this repository](https://github.com/ammitra/timber-docker). Included in the instructions are more examples and tips on learning how to use ROOT.

</details>

# Structure of the event selection exercises

<details>
<summary>Description of the first three exercises</summary>
<br>

The first three exercises will be based on three python scripts, with further instructions written in the repo's exercies/README.md file:

* [`exercises/exercise1.py`](https://gitlab.cern.ch/cmsdas-cern-2024/long-ex-b2g-bstar/-/blob/master/Event_Selection/exercises/exercise1.py?ref_type=heads)
* [`exercises/selection.py`](https://gitlab.cern.ch/cmsdas-cern-2024/long-ex-b2g-bstar/-/blob/master/Event_Selection/exercises/selection.py?ref_type=heads)
* [`exercises/nminus1.py`](https://gitlab.cern.ch/cmsdas-cern-2024/long-ex-b2g-bstar/-/blob/master/Event_Selection/exercises/nminus1.py?ref_type=heads)

These scripts have been written to interact with NanoAOD Monte Carlo simulation files for background and signal using TIMBER. The file `exercise1.py` will teach you to how to use the TIMBER framework to define new variables in the RDataFrame, make cuts, and plot distributions all by practicing on one signal mass point. We will play around with this file to investigate which kinematic cuts to make and how they impact the signal distribution. Once you've determined which cuts you'd like to make, you can try it out on `selection.py`, which is largely identical to `signal.py` but allows you to perform the event selection on multiple signal mass points as well as on the $t\bar{t}$, single-top, and QCD multijet Monte Carlo background samples.

The file `nminus1.py` is used to illustrate the important concept of N-1 ("N minus 1") plots in event selection. This script defines several cuts to make on the input dataset and then generates the N-1 plots from them. The idea of an N-1 plot is to gather cuts on N variables in your dataset and then, for each of the N variables, perform **all cuts but the one on that variable** and plot the distribution of the variable in question. By doing so, you can analyze the impact (or lack thereof) of any of the cuts in your event selection on the relevant kinematic distributions.

**Note:** the `exercise1.py` file will automatically plot the results of your event selection on the single signal mass point. The `selection.py` script takes in as command-line input the name and year of whatever sample you'd like to run selection on. It will not automatically plot the results but instead generate an output ROOT file containing any histograms you choose to generate in the script. These histograms can then be plotted for all backgrounds and signals of interest using `exercises/plot.py`.

Our input files are a skim of the official NanoAOD production (skim in this context means fewer variables have been saved than were originally in the file and that some post-processing has happened - such as the implementation of jet energy corrections). Sometimes, samples are ‘slimmed’ (removal of unnecessary information/branches) or ‘skimmed’ (application of loose selections to reduce number of events) in order to reduce the size of the sample files. The input files for this exercise are located on the CERN EOS under `/eos/user/c/cmsdas/2024/long-ex-b2g-bstar/rootfiles/`.

Any plots that are created by these scripts will be stored in the `plots/` directory.

Any ROOT files that are created (to store histograms, skims, etc) by these scripts are stored in the `rootfiles/` directory. In this directory are also some premade example outputs you can look at and use. 

</details>

# Exercise 1

<details>
<summary>Basics of the event selection</summary>
<br>

Before letting you work on the analysis independently, let's go through an exercise on how to implement an event selection in TIMBER. This exercise will cover the following:

* How to define a custom function for a new variable
* How to select on existing variables (jet $p_{T}$)
* How to select on booleans (event MET filters, trigger booleans)
* How to re-weight events (e.g. applying normalization weights) 

The script for this exercise is [`exercises/exercise1.py`](https://gitlab.cern.ch/cmsdas-cern-2024/long-ex-b2g-bstar/-/blob/master/Event_Selection/exercises/exercise1.py?ref_type=heads) - it takes as input one signal sample: a left-handed $b^\ast$ of mass 2 TeV.

We will first go over the script together, understand the RDataFrame usage and the custom functions in TIMBER, and then run it.

You can run it by typing into the shell:

```
python exercises/exercise1.py -y 16 --select
```

The `--select` flag indicates to the script that you want to perform the entire event selection and save the results to a new ROOT file. Running the script without the `--select` flag will not perform the selection and instead read the output ROOT files and generate histograms of the variables of interest. The plots are outputted to the `plots/` directory.

A fun thing to use in TIMBER is the `PrintNodeTree()` functionality, which allows you to print a graphical representation of the definitions and cuts that you applied to your input file. In the script, the graphical representation is saved in the `.dot` format. You can visualize it using `graphviz`, i.e. by converting the `.dot` file to `.png` with:

```
dot plots/exercise1.dot -T png -o plots/exercise1.png
```

The plot can be viewed over SSH via `display plots/exercise1.png` (if you've enabled X11 forwarding via `ssh -Y` when logging in to LXPLUS) or by copying it to your local machine and viewing it:

```
scp -r [USER]@lxplus8.cern.ch:~/public/long-ex-b2g-bstar/Event_Selection/plots/exercise1.png .
```

**Your task:** Define the variable `lead_softdrop_mass`, representing the mass of the leading `FatJet` (AK8 jet) in every event, select only on `lead_softdrop_mass` values greater than 50 GeV, and then plot the result. *Hint:* You can build off of the examples in `excercise1.py`.

</details>

# Exercise 2

<details>
<summary>Investigating the signal topology</summary>
<br>

One of the first questions we should ask ourselves is **what does our signal look like?**. Understanding the answer to this will help us decide what selections we should apply to keep as much of the signal as we can, while rejecting as much background as possible. We already starting exploring the signal sample with the last exercise, but let's get ourselves acquainted with all the information that is available to us.

To view the information stored inside a sample, open it up with ROOT and check out its branches. In the below code, we open a signal sample file located on the CERN EOS, passing the `-l` flag to open the file without displaying the graphical ROOT banner. We then create a pointer to the `Events` TTree in the file, naming it `myTree`. From this point on, we can act on the `Events` tree by using the arrow operator (`->`) on the `myTree` pointer, which acts to dereference the pointer to the TTree and utilize some of its member functions, namely `Print()`, `Scan()` and `Draw()`. The `Print()` function simply prints a summary of the TTree structure, i.e. what branches it contains. The `Scan()` function prints the contents of a TTree's entries which pass a selection. In the below example, the selection is on AK8 jets with a $p_T$ less than 400 or 300 GeV:

```
root -l /eos/user/c/cmsdas/2024/long-ex-b2g-bstar/rootfiles/signalLH2000_bstar16.root
.ls
TTree* myTree = (TTree*) _file0->Get("Events")
myTree->Print()
myTree->Scan("FatJet_pt")
myTree->Scan("FatJet_pt","FatJet_pt<400")
myTree->Scan("FatJet_pt","FatJet_pt<300")
myTree->Draw("FatJet_pt")
```

There are more sophisticated ways to investigate the branches available as well as their min/max values in root and python. For example, you can write a script that plots all the variables in the tree and determine from there the min/max values of each.

**Question:** What kind of variable would be most useful for our signal identification, given that our signal is a resonance? And how would you build it?

<details>
<summary>Solution</summary>
<br>

For our signal, the mass of the $b^{\ast}$ is also an observable that could help discriminate; we can reconstruct the $b^{\ast}$ effective mass (mtW) from the decaying top quark and W boson. In this case we have not yet reconstructed the top or the W but we can assume that they are the sub-leading and leading jets in the event, respectively, and use them to create and look at the distribution of the invariant mass.

As a first step, let's take a look at some properties of the leading and subleading jets in our signal sample:

```
myTree->Draw("FatJet_msoftdrop[0])
myTree->Draw("FatJet_msoftdrop[1])
```

You can then plot them together by running:

```
Draw("FatJet_msoftdrop[1]:FatJet_msoftdrop[2]","","COLZ")
```

You can check out the ordering of events by $p_T$ by running:

```
myTree->Draw("FatJet_pt[0])
myTree->Draw("FatJet_pt[1])
myTree->Draw("FatJet_pt[2])
myTree->Draw("FatJet_pt[3])
```

</details>

**Your task:**  In our analysis introduction (and in the paper) we defined some variables that are incredibly useful for us to identify the signal for W-tagging and top-tagging. Please include the following variables in the `exercies/exercise1.py` script for the leading and sub-leading jets:

```
FatJet pt, softdrop mass, tau21, tau32, deepAK8_Wscore, deepAK8_topScore
```

and finally, define and add the invariant mass of the leading and sub-leading jet system (this will be an initial, naive estimate of the invariant mass of the $b^{\ast}$ resonance.

Hints on how to include these variables are in the `exercises/selection.py` script. 

</details>

# Exercise 3

<details>
<summary>Event pre-selection</summary>
<br>

A preselection serves two purposes - first to ensure that passing events only utilize a “good” region of the detector with appropriate noise filters, and second to start applying simple selections motivated by the physics of the signal topology.

## Defining a “good” region of the detector

A “good” region of the detector depends heavily on the signal topology. The muon system and tracker extend to about $|\eta| = 2.4$ while the calorimeter extends to $|\eta| = 3.0$ with the forward calorimeter extending farther. Thus, if the signal topology relies heavily on tracking or muons, then a useful preselection would limiting the region to $|\eta| < 2.4$. Some topologies, like vector boson fusion (commonly called VBF) have two forward (high $\eta$) jets, so a cut that requires two forward jets is a useful preselection.

Now, let’s take a more detailed look at our signal topology and see how it fits in with the detector. The $b^{\ast}$ is produced from the interaction of a bottom quark and a gluon - will this production mode yield any characteristic forward jets? In this topology, the $b^{\ast}$ decays to a jet from a W boson and a jet from a top quark. What is characteristic of a top jet? What about a W jet? How does this impact the region of the detector needed?

**Question:** What $|\eta|$ and $\varphi$ in the detector do we need? Think about this while looking at the Feynman diagram and the signal topology..

<details>
<summary>Solution</summary>
<br>

The production mode does not have any characteristic forward jets, but the final state has two jets. The top quark decays to a b jet and W jet, where the b jet is typically identified by making use of its characteristic secondary vertex. This secondary vertex is identified in the tracker. Both the W jet and top jet have unique substructure that can be used to distinguish them from QCD jets. Therefore it is crucial to use a region of the detector with good tracking and granular calorimetry, so we should restrict $|\eta| < 2.4$. There are no detector differences in $\varphi$ that should impact this search, so there should be no restriction in $\varphi$.

</details>

## Finding MET filters

Missing transverse momentum (called MET) is used to identify detector noise and MET filters are used to remove detector noise. The MET group publishes recommendations on the filters that should be used for different eras of data. The recommended MET filters for Run II are listed on this [twiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/MissingETOptionalFiltersRun2).

## Simple selections

The preselection should also include a set of simple selections based on our physics knowledge of the signal topology. These “simple” selections typically consist of loose lower bounds only, which help to reduce the number of events which will get passed to the rest of the analysis while still preserving the signal region.

Consider the decay of our heavy $b^{\ast}$ resonance into a W and a top jet. The heavy resonance with little longitudinal momentum, so conservation of momentum tells us that the jets should be well separated in $\varphi$; ideally they should have a separation of $\pi$ in $\varphi$. Therefore, placing a selection of $\Delta\varphi > \pi / 2$ should not cut out signal, but will reduce the number of events passed on to the next stage.

**Question:** Why not use a selection close to $\Delta\varphi = \pi $?

<details>
<summary>Solution</summary>
<br>

The jets can recoil off of other objects creating a dijet pair that will not result in $\Delta\varphi = \pi / 2$.

</details>

This also a good stage to place a lower limit on the jet $p_T$. In a hadronic analysis, it is common to place a high lower limit on the $p_T$. For this example, a lower limit of $p_T > 400$ GeV should be good.

**Question:** Why place such a high lower limit on jet $p_T$?

<details>
<summary>Solution</summary>
<br>

This is a tricky question. It relates to the trigger. Hadronic triggers have a turn on at high $H_T$ or high $p_T$, so the lower limit ensures that that the analysis will only investigate the fully efficient region.

</details>

We can also start selecting the "prong-iness" of the jets. For a jet originating from the hadronic decay of a W boson we expect two “prongs”, which we can use to place an upper limit on the $\tau_{21}$ ratio and for a top jet we can place an upper limit on the $\tau_{32}$ ratio.

Finally, we can use the softdrop mass (this algorithm will help to reduce the amount of pileup that is used when measuring the jet mass). The preselection is a good place to define a wide softdrop mass region. For this example, a wide region around the W boson mass would be ideal, such as $65 < \text{mSD} < 115$ GeV. We need to define something similar around the top mass region.

## Your task

Based on the previous discussion, you should have an understanding of the event selection criteria you want. Modify the `exercises/exercise1.py` script to include the new cuts and definitions and plot the relevant variables.

Now that we've investigated the signal, it's time to look at the backgrounds as well. The script `exercises/selection.py` should contain all the event selection filters that you need. To use the script, run

```
python exercises/selection.py -s [SETNAME] -y [16/17/18]
```

Where the possible setnames (here, a "set" just refers to a Monte Carlo file for a given physical process) are:

* `QCDHT700/1000/1500/2000`
* `signalLH*` where the masses range from 1200 to 4000 in steps of 200 GeV
* `ttbar`
* `singletop_tW`
* `singletop_tWB`

The script will run the event selection and save histograms of the variables defined in the script and listed in the `varnames` dictionary. You can (and should) of course define your own variables and add them to the dictionary and they will be plotted. **NOTE: the QCDHT datasets are very large, and running event selection on them will take a while. To alleviate this issue, it might be beneficial to split up the jobs among each other.**

In order to run over all datasets and all years, you can use the `run_all_datasets.py`.
To use the script, run

```
python exercises/run_all_datasets.py --select
```

If you want to run over just one year, you can run
```
python exercises/run_all_datasets.py --select -y [16/17/18]
```

After you've run the event selection on the signal and backgrounds, you can plot them with `exercises/plot.py -y <16/17/18> [--logy]`, where the optional `--logy` flag will make the y-axis logarithmic. If you've made any other variables and saved them to the output ROOT files, make sure you add them to the `varnames` dictionary inside `exercises/plot.py` so that they'll be plotted as well.

**Question:** What criteria would you use for `Tau21`? What about `Tau32`?

**Question:** Is the $p_T$ of the lead and sublead jet a sufficient enough identifier of the top and W candidates?

</details>

# Exercise 4

<details>
<summary>Optimize the selection</summary>
<br>

In the last exercise, we talked about how a preselection is useful to cut down the size of the ntuples produced/read. Generally your preselection should include the union of your signal and control regions, cutting out unnecessary data. Later on, we will make further selections to optimize our signal regions and estimate the background in control regions. For this stage, using the plots we were able to make from `exercises/selection.py`, let’s decide on a preselection.

**Question:** What should our preselection be? Discuss for the next ~15 minutes what the preselection for our analysis should be. Use your plots as evidence supporting your argument.

<details>
<summary>Solution</summary>
<br>

The pre-selections chosen for the all-hadronic $b^{\ast} \to tW$ (B2G -19-003) analysis were:

* Standard MET filters and [jetID](https://twiki.cern.ch/twiki/bin/view/CMS/JetID)
* $p_{T}(t)$, $p_{T}(W)$ > 400 GeV
* $|\eta| < 2.4$
* $|\Delta\varphi| > \pi/2$
* $|\Delta y| < 1.6$
* W-tag: $\tau_{21} < 0.4/0.45$ and $65 < m_{\text{SD}} < 105$ GeV
* (later in the analysis) $\text{mtW} > 1200$ GeV

</details>

With an idea of how we want to make our preselection, let’s take a moment to think ahead to how we want to organize our analysis. We know we will want to use $\text{mtW}$, so we need to make a rough decision of how we are going to select for signal and estimate the background.

**Question:** What is our general signal-selection/background-estimation strategy going to be? Think about what backgrounds we have, how we will estimate those, and how we can make the best selection for a signal region.

<details>
<summary>Solution</summary>
<br>

We can define the signal region by further optimizing selections on our top and W candidate jets, with a window selection on the top mass, $m_t$ and perform the bump-hunt in $\text{mtW}$.
</details>

Perhaps when deciding the rough preselection cuts you may have already thought "How do I make the best cuts to the variables available to me?" One (bad) way to define whether a selection is ‘good’ or ‘bad’ is by simply asking if it cuts away more signal than it does background. The reason this is not adequate is that you want to make a selection that cuts away background at a rate that enhances the signal significance.

Roughly, the significance of the signal strength can be estimated as the ratio of the signal over the square root of the dataset: 

```
sigma ~ N_sig/ Sqrt(N_tot) ~  N_sig/ Sqrt(N_bkg)
```

Going forward with this exercise, we will use the S/√B approximation for significant to guide our decisions.

Now that we understand what the minimal selection is that we want to apply to our signal and background, we need to think harder about what are the final (tighter) selections that we want to apply to define our signal and control regions.

**Your task:** Re-run the plotting but now pass the `--soverb` flag to have TIMBER automatically calculate and plot the signal significance:

```
python exercises/plot.py -y <16/17/18> --soverb
```

**Question:** What are some parts of the analysis you think could be optimized? In addition to the preselection, we can make selections on the top and W bosons to ensure an enriched signal region. As mentioned above, the pre-selections chosen for the all-had $b^{\ast} \to tW$ (B2G-19-003) analysis were:

<details>
<summary>selection</summary>
<br>

* Standard MET filters and [jetID](https://twiki.cern.ch/twiki/bin/view/CMS/JetID)
* $p_T(t)$, $p_T(W)$ > 400 GeV
* $|\eta| < 2.4$
* $|\Delta\varphi| > \pi/2$
* $|\Delta y| < 1.6$
* W-tag: $\tau_{21} < 0.4/0.45$ and $65 < m_{\text{SD}} < 105$ GeV
* (later in the analysis) $\text{mtW} > 1200$ GeV

</details>

Our selections should at-least be this tight. In addition to these selections, the SR of their analysis is defined by a top tag of:

* $\tau_{32} < 0.65$
* $m_{\text{SD}}$ window of [105, 220] GeV
* `deepCSVsubjet < 0.22(2016), 0.15(2017), 0.12(2018)`

More specifically, the B2G-19-003 team included the mtW > 1200 GeV cut in the later selections and defined the signal region (SR) as having one two jets that pass the preselection cuts, where one jet is W-tagged but not top-tagged, and the other jet is top-tagged but not W-tagged. In the case that an event contains two leading jets which are top-tagged, the event was used in a $t\bar{t}$ measurement region. To generate a control region (CR) that is enriched in multijet background, the second jet must be "anti" top-tagged. This will be explained later, during the background estimation portion of this exercise.

Do we want to use similar selections? Try looking into these distributions and make your own call! 

## N minus 1 plots

One powerful analysis tool for optimization are what are referred to as ‘N minus 1’ plots. These are plots of distributions used in series of selections, systematically omitting one selection of the series at a time and plotting that variable. N-1 plots can help us understand the impact of tightening cuts on the variable. Normally, we ‘tighten’ selections and want to know how our significance estimate changes as a function of ‘tightening’. The direction of ‘tight’ depends on the observable at hand, for example toward 0 for $\tau_{32}$ and towards infinity for jet $p_{T}$.

**Question:** How would you define 'tight' for a mass peak?

<details>
<summary>Solution</summary>
<br>

Normally we use a one-sided selections, but for a mass peak we may want to make a window requirement.

The closer you are to the target mass the tighter the cut would be considered. Thus, you would have to make a decision on what the target mass should be (note this could differ from the ideal mass) for use as the upper limit of the significance estimation. 

</details>

Give it a shot yourself, first generate the N-1 distributions by running:

```
python exercises/nminus1.py -s [SETNAME] -y [16/17/18]
```

where the setnames are exactly the same as before. 

In order to run over all datasets and all years, you can use the `run_all_datasets.py`.
To use the script, run

```
python exercises/run_all_datasets.py
```

If you want to run over just one year, you can run
```
python exercises/run_all_datasets.py -y [16/17/18]
```

To plot the distributions of signal and background, run:

```
python exercises/plot.py -y [16/17/18] --nminus1 [--logy] [--soverb]
```

This should create some example plots for the selections used by the B2G -19-003 analysis team. Change the selections applied to what you have decided upon today to checkout the impact of your cuts. Is your selection optimal? Use the remaining time to produce and look into these plots to come up with a signal region selection.

Finally, let's take a look into two of the most important observables of the signal: the invariant mass of the t+W system, and the mass of the top jet. 

**Question:** Before plotting, what do you think the distribution should look like, and why?

</details>


