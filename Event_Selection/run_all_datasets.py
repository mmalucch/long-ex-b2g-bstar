import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-y", "--year", type=str, help="Year (16,17,18)")
parser.add_argument(
    "--select",
    action="store_true",
    help="Whether to run the selection or nminus1",
    default=False,
)
args = parser.parse_args()

qcd_list = ["QCDHT700", "QCDHT1000", "QCDHT1500", "QCDHT2000"]
signal_list = ["signalLH{}".format(mass) for mass in range(1200, 4200, 200)]
top_list = ["ttbar", "singletop_tW", "singletop_tWB"]

datasets = qcd_list + signal_list + top_list

years = ["16", "17", "18"]

os.system("mkdir -p logs")

for year in years:
    if args.year and args.year != year:
        continue
    for dataset in datasets:
        if args.select:
            print(f"Running selection for {dataset} in year {year}")
            os.system(f"nohup python exercises/selection.py -s {dataset} -y {year}  > logs/log_{dataset}_{year}.log &")
        else:
            print(f"Running N-1 for {dataset} in year {year}")
            os.system(f"nohup python exercises/nminus1.py -s {dataset} -y {year}  > logs/log_{dataset}_{year}.log &")
