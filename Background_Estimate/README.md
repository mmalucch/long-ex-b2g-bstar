# Long exercise part 2: Background Estimation
The code in this directory is used for the second part of the B2G long exercise - the background estimation. 

# Getting started (Alma Linux 8)

<details>
<summary>First-time installation</summary>
<br>

The following instructions assume that you have already cloned this main exercise repository following the `README` in the base directory (e.g., under `~/public/long-ex-b2g-bstar/`) and that you have added [SSH keys](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account) to your github account.

These are the instructions to set up the background estimation tools on `el8` and `el9` architectures. **NOTE:**

```
# Log in, move to exercise directory
ssh -Y USERNAME@lxplus8.cern.ch
cd ~/public/long-ex-b2g-bstar/Background_Estimate/

# Set up Combine
cmssw-el7
cmsrel CMSSW_11_3_4
cd CMSSW_11_3_4/src
cmsenv
git clone https://github.com/cms-analysis/HiggsAnalysis-CombinedLimit.git HiggsAnalysis/CombinedLimit
cd $CMSSW_BASE/src/HiggsAnalysis/CombinedLimit
git fetch origin
git checkout v9.2.1

# Set up Combine Harvester
cd ../../
git clone git@github.com:JHU-Tools/CombineHarvester.git
cd CombineHarvester/
git fetch origin
git checkout CMSSW_11_3_X
cd ../

# Build
scramv1 b clean
scramv1 b -j 8

# Install 2DAlphabet
git clone https://github.com/JHU-Tools/2DAlphabet.git
python3 -m virtualenv twoD-env
source twoD-env/bin/activate
cd 2DAlphabet/
git fetch origin
git checkout el8-el9-py3
python setup.py develop
cd ..
```

You can test that the 2DAlphabet installation is working by running the following in your shell:

```
python -c 'import ROOT; r = ROOT.RooParametricHist()'
```

If all went well, the command should be executed with no output.

</details>

<details>
<summary>What to do every time you log in to LXPLUS, after installation</summary>
<br>

You do not need to re-compile the packages every time you log back in to the node. Instead:

* Navigate to the directory where you started the apptainer container.
   * In our case, this is `~/public/long-ex-b2g-bstar/Background_Estimate/`
* Start the container with `cmssw-el7`
* `cd CMSSW_11_3_4/src/`
* `cmsenv`
* `source twoD-env/bin/activate`

And then you are ready to work with 2DAlphabet + Combine. Once you have sourced the python virtual environment, you can `cd ../../` back up to `~/public/long-ex-b2g-bstar/Background_Estimate/` where the exercise files are located.

You will need to perform this step every time you log into the LXPLUS cluster.

</details>

# 2DAlphabet information

<details>
<summary>The software</summary>
<br>

For this exercise we will use the [2DAlphabet](https://github.com/JHU-Tools/2DAlphabet/tree/el8-el9-py3) github package. This package uses `.json` configuration files to specify the input histograms (to perform the fit) and the uncertainties. These uncertainties will be used inside of the Higgs Combine backend, the fitting package used widely within CMS. The 2DAlphabet package serves as a nice interface with Combine to allow the user to use the 2DAlphabet method without having to create their own custom version of combine.

</details>

<details>
<summary>The 2DAlphabet configuration file</summary>
<br>

The configuration file that you will be using in this exercise is called `bstar.json`, located in this repository. Let's take a look at this file and see the various parts:

* `GLOBAL`
   * This section contains meta information regarding the location (`path`), filenames (`FILE`), and input histogram names (`HIST`) for all ROOT files used in the background estimation procedure.
   * Everything in this section will be used in a file-wide find-and-replace. So wherever you see the name of the sub-objects in this file, it will be expanded by the value assigned to it in this section.
   * Additionally, the `SIGNAME` list should include the name(s) of all signals you wish to investigate, so that they are added to the workspace when you run the python script.
    * If you wanted to investigate limits for only three signals, for example, you'd just add their names as given in the ROOT files to this list.
    * For this exercise, the default is `signalLH2400`, the 2.4 TeV signal sample. You'll want to change this as the exercise progresses
* `REGIONS`
   * This section contains the various ABCD regions we are interested in transferring between.
   * Each region contains a `PROCESSES` object, listing the signals and backgrounds to be included in the fit, as well as `BINNING` object, which is defined elsewhere in the config file.
   * The name of each region in `REGIONS` is dependent on the input histogram name, as well as your choice of `HIST` name in the `GLOBAL` section above
    * For instance, in this file we declared `HIST = MtwvMt$region`, where `$region` will be expanded as the name given in `REGIONS`.
    * We chose this name because the input histograms are titled `MtwvMtPass` and `MtwvMtFail` for the Pass and Fail regions, respectively.
* `PROCESSES`
   * In this section we define all of the various process ROOT files that will be used to produce the fit. These include data, signals, and backgrounds.
   * Each process contains its own set of options:
    * `SYSTEMATICS`: a list of systematic uncertainties, whose properties are defined elsewhere in the config file
    * `SCALE`: how much to scale this process by in the fit
    * `COLOR`: color to plot in the fit (ROOT color schema)
    * `TYPE`: `DATA`, `BKG`, `SIGNAL`
    * `TITLE`: label in the plot legend (ROOT LaTeX compatible)
    * `ALIAS`: if the process has a different filename than standard, this will be what replaces `$process` in the `GLOBAL` section's `FILE` option, so that this process gets picked up properly
    * `LOC`: the location of the file, using the definitions laid out in the `GLOBAL` section
* `SYSTEMATICS`
   * This contains the names of all systematic uncertainties you want to apply to the various processes.
   * The `CODE` key describes the type of systematic that will be used in Combine.
    * `0`: (symmetric, lnN)
    * `1`: (asymmetric, lnN)
    * `2`: (shape uncertainty, and contained in same file as nominal)
    * `3`: (shape uncertainty, and NOT in same file as nominal)
   * Normalization uncertainties:
    * The `VAL` key is how we assign the value of that uncertainty. For instance, a `VAL` of 1.018 in the `lumi` (luminosity) means that this systematic has a 1.8% uncertainty on the yield. **NOTE:** The `VAL` key is used only for normalization uncertainties, i.e. uncertainties which do not affect the shape of the distribution. 
   * Shape uncertainties:
    * The `UP/DOWN` keys point to the location of the histograms of the distributions subject to $\pm 1\sigma$ changes in the given systematic. The locations should be given using the `GLOBAL` tags, i.e. `"UP": "path/FILE:HIST_UP"` and `"DOWN": "path/FILE:HIST_DOWN"`. If the templates are stored in different *files* rather than different *histograms*, then one can use `"UP": "path/FILE_UP:HIST"` and `"DOWN": "path/FILE_DOWN:HIST"`. The values `FILE_UP/DOWN` must be defined in the `GLOBAL` section for this to work. 
* `BINNING` 
   * 2DAlphabet performs a binned likelihood along two dimensions, where the analyst can choose which observables are fit and plotted along the `X` and `Y` axes. This section allows us to name and define custom binning schema. After naming the schema, one would define several variables for both `X` and `Y` axes.:
    * `NAME`: allows us to denote what is being plotted on the given axis
    * `TITLE`: the axis label for the plot (ROOT LaTeX compatible)
    * `BINS`: a list of bin edges which will be used in the fitting as well as plotting. They can be the same as the original input histogram bins or a subset, but the edges *must* exist in the original input histograms. 
   * **FOR THE `X`-axis ONLY:**
    * The x-axis is designed to be able to be blinded along a signal window, and so for the `X` section of the `BINNING`, one must use `SIGSTART` and `SIGEND` to define a window `[SIGSTART, SIGEND]` around which to blind (if blinding is desired)
* `OPTIONS`
   * A list of boolean and other options to be considered when fitting and plotting
    * `blindedPlots`: A list of bin edges to blind on the plots (**NOTE:** this will *not* automatically blind the fits)
    * `blindedFit`: a list of bin edges to blind during the fit
    * `plotPrefitSigInFitB`: boolean, whether to plot the prefit signal distributions in the background-only fit plots for a reference of an expected signal distribution
    * `plotTemplateComparisons`: boolean, whether to plot the nominal/up/down distributions of all processes subject to shape uncertainties in 1D along both fit axes in a separate plotting directory.
    * `plotTitles`: boolean, whether or not to include titles in the plots

</details>

# Introduction

<details>
<summary>Learning objectives:</summary>
<br>

In this section of the exercise we hope you learn:

* The main backgrounds of this analysis
* The fundamentals of a maximum-likelihood fit
* The concept of the ABCD (Alphabet) method for background estimation, and extending it to 2 dimensions
* How and why we perform a data-driven estimate of the QCD multijet background
* What are systematic uncertainties? Why must they be included, and which?
* What are asymptotic limits, and how to use Combine + 2DAlphabet to obtain them 

</details>

<details>
<summary>Summary of backgrounds</summary>
<br>

Let's first understand the backgrounds that can lead to a top+W-like experimental signature:

* **Multi-jet QCD:** The nature of the LHC means hadronic background is given for any analysis that does not have a lepton or missing energy to flag. The reason we get large masses in W/t-candidates is that jets cluster any PF candidate within the R-parameter, and with enough momentum and/or distance between PF candidates these QCD events can create a large-mass jet.
* **W+jets:"** This process is when a single W-boson is produced in association to QCD jets. Since the preselection identifies the true W, the process become a background to our signal through its associated jets. Thus, we can include the process as part of the ‘QCD’ (stochastic jets) background contribution.
* **ttbar:** The simplest way this process can enter our signal region is a fully-hadronic decay, where one of the b-quarks is either soft or not identified with our b-tagging. It can also happen that the top has a large decay opening-angle and the W decay is captured in a large-R jet separate from the associated b-quark.
* **single-top:** The Standard Model can have a pair of protons produce a single top in association to a (usually heavy) quark or W-boson. In our case the top produced with a W becomes an irreducible background, however less dominant than the above processes. See [here](https://arxiv.org/pdf/2109.01706.pdf) for a paper regarding single-top production at the CMS.

</details>

<details>
<summary>Mandatory: crash course on fitting</summary>
<br>

Before we start with our background estimation section. let's take a review of concepts about fitting. Lucas Corcodilos (the author of both TIMBER and 2DAlphabet, and the analysis on which this exercise is based!) prepared these nice slides linked [here](https://docs.google.com/presentation/d/1-a-BdsdvwMGYuqYZfHHA5hl7fp6ns5WTGiWjNdWYv2g/)

</details>

<details>
<summary>Control regions</summary>
<br>

Just as signal regions are defined by selections meant to enrich a measurement in signal while eliminating as much background as possible, we define control regions (CR's) by selections meant to enrich a measurement with a particular background and rejecting as much signal as possible. The idea behind control regions is to provide a kinematic region which is orthogonal but kinematically ‘close’ to the signal region, where a particular background is dominant. The control region can be used to correct and constrain uncertainties on a background process modeled with MC, or can be used to derive an estimate of the background in a fully data-driven way. Even though control regions are chosen to be similar to the signal region, one still needs to model the differences between the two based on some sort of extrapolation. The extrapolation could be a single ‘Scale Factor’, or a more sophisticated ‘Transfer Function’.

Recall that, since the preselection first identifies one of the two candidate jets as having originated from the W, our QCD multijet-enriched control region can then be defined by events whose second candidate jet is "anti" top-tagged, i.e., likely to have originated from QCD processes. A $t\bar{t}$-enriched control (or measurement) region can be created using events in which both candidate jets are top-tagged. Finally, the signal region is of course populated by events with one W-tagged jet and one top-tagged jet.

In the actual analysis, the B2G -19-003 team had four selection regions considered in the likelihood fit to data. They are summarized in the table below, where the columns indicate the possible jet tag for the first jet considered in the preselection while the rows indicate the possible jet tag classification for the second candidate jet.

|         | W tag | Top tag |
| Top tag | Signal region (SR) | ttbar measurement region (MR) |
| "Anti" top tag | Multijet control region (for SR) | Multijet control region (for MR) |

</details>

<details> 
<summary>Background estimates with transfer functions</summary>
<br>

QCD multijet backgrounds are notoriously difficult to model. Therefore, instead of using Monte Carlo simulations to model the multijet contribution to the background estimate, a data-driven estimate is employed. In a data-driven estimate, well-modeled processes ($t\bar{t}$ production, etc.) are subtracted from the data to create a template of the poorly-modeled (e.g. QCD multijet) background. Then, since the background distributions in the control region and signal region are correlated, we can extrapolate the shape of the QCD background in the SR by using its shape in the CR.

Normally, we do a bump hunt along some observable and measure our background in near-by kinematic regions to estimate its expected yield in the signal region. In our case, we will consider the entire two-dimensional (m_t, m_tW) plane, and designate a window in $m_t$ close to the top mass as our signal region and everywhere else as a control region. We can then use the information from the corresponding window in the control region to fit a smooth function which will extrapolate over our SR window.

This method, called 2DAlphabet, is explained in detail in the presentation linked [here.](https://twiki.cern.ch/twiki/pub/CMS/SWGuideCMSDataAnalysisSchoolLPC2021LongExerciseB2G/2DAlphabetCMSDAS.pdf)

</details>

<details>
<summary>A note about blinding</summary>
<br> 

One thing to keep in mind when using data before settling on final signal and control region selections is the possibility of biasing one’s methods by being exposed to the data. For example, one could imagine a situation where one is ‘testing’ methods in a signal region and then observes a ‘problem’ (lack of events, too many events, etc) which guides the analyzer to biased decision-making. A similar problem can occur if a control or validation region is chosen that has significant signal contamination.

To avoid these problems, we ‘blind’ our data whenever possible. That is, we do not look at any data in the SRs - when appropriate we could also decide to remove data if it passes a significance threshold, and ensuring the control/validation regions have little signal contamination. 

</details>

# Exercise 5: Understanding 2DAlphabet

<details>
<summary>Getting started</summary>
<br>

2DAlphabet uses a JSON configuration files to specify the input histograms (to perform the fit), systematic uncertainties, and binning/plotting options for the fit. The values stored in the JSON configuration file will be used inside of a Python script that calls the [Combine](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/latest/) backend, the fitting package widely used in CMS. The 2DAlphabet package therefore serves as a nice interface with Combine to allow the user to use the 2D "alphabet" method without having to create their own custom version of Combine.

The configuration files that we will be using are located in this repository under either `bstar.json` (for top/W tagging performed using the `tau32` and `tau21` discriminants, respectively) or `bstar_deep.json` (for top/W tagging performed using the DeepAK8 tagger discriminants).

The Python script we'll be using is `bstar.py`. This script is set up to perform the maximum likelihood fit, plot the results, and then run limits.

**Task 1:** Take a look at the `bstar.json` file and read the "2DAlphabet information" section above to familiarize yourself with the setup

For this exercise we will be using pre-made files containing the 2D histograms which serve as an input to the background estimate. The files are stored on the CERN EOS under `/eos/user/c/cmsdas/2023/long-ex-b2g/rootfiles/`.

**Task 2:** Look at the files on the EOS by running `ls /eos/user/c/cmsdas/2023/long-ex-b2g/rootfiles/`. Compare the format of the file names to what is contained in the `GLOBAL` section of `bstar.json`.

**Task 3:** Open up one of the files, e.g. `root -l /eos/user/c/cmsdas/2023/long-ex-b2g/rootfiles/TWpreselection18_signalLH2400_tau32medium_default.root`. This file contains the 2D histograms of the left-handed $b^\ast$ with a mass of 2.4 TeV in the Fail and Pass regions of the analysis. Compare the format of the histogram names within the file to what is described in the `GLOBAL` section of `bstar.json`. 

**Task 3:** Take a look at the corresponding `bstar.py` script and read through the comments to understand what is being performed at each step.

</details>

# Exercise 6: Running 2DAlphabet

<details>
<summary>Now that we understand how to set up 2DAlphabet, let's try running it.</summary>
<br>

For this exercise and the remaining, we'll use `bstar.py` with the configuration file `bstar.json`. At the moment, `bstar.json` is pretty barebones - we will fill it in over the course of this exercise. 

Currently the JSON file contains the following processes for 2016, 2017, and 2018:

* `SIGNAME` (the signal provided is `signalLH2400`)
* `ttbar` (all-hadronic ttbar)
* `ttbar-semilep`
* `singletop_tW`
* `singletop_tWB`

**Question:** Which systematic uncertainties are applied to each of these processes, currently, and what are their values?

<details>
<summary>Solution</summary>
<br>

To the signal, only a luminosity uncertainty is applied. To the `ttbar` and `singletop` processes, both a luminosity and cross-sectional uncertainty is applied. The luminosity uncertainty is 1.8% and the cross section uncertainties range from 20-30%. These values are defined in the `SYSTEMATICS` section:

```
    "SYSTEMATICS": {
        "lumi": {
            "CODE": 0,
            "VAL": 1.018
        },
        "ttbar_xsec": {
            "CODE": 0,
            "VAL": 1.20
        },
        "st_tW_xsec": {
            "CODE": 0,
            "VAL":1.30
        },
        "st_tWB_xsec": {
            "CODE": 0,
            "VAL":1.30
        },
```

</details>

**Bonus Question:** The uncertainties currrently applied to the processes in the JSON file have a `CODE` value of `0`, indicating that they are `lnN` or [Log-normal](http://en.wikipedia.org/wiki/Log-normal_distribution) uncertainties. Take the luminosity uncertainty of 1.8% - by which amounts are the yields multiplied for $\pm 1\sigma$ variations in this correction?

<details>
<summary>Solution</summary>
<br>

The relative uncertainty in the $t\bar{t}$ uncertainty correction can be written as $\Delta x/x = 0.2$. Combine looks for the log-normal value $\kappa = 1 + \Delta x/x$, which equals `1.20` as written in the JSON. For this value, the yield of the process it is associated with is multiplied by $\kappa^\theta$, where $\theta$ represents how many standard deviations from the nominal we vary the paramater. 

At $\theta = 0$ the nominal yield is retained, at $\theta = 1\sigma$ the yield is multiplied by $\kappa$, and at $\theta = -1\sigma$ the yield is multiplied by $1/\kappa$. 

Therefore an uncertainty in the ttbar cross-section represented as `1.2` does not multiply the nominal yield by 0.8 for $\theta = -1\sigma$ but by 0.8333. In this way the ttbar cross section uncertainty parameter can never result in a negative mulitplicative correction to the yields, which would be unphysical and would also cause errors in the fit.

</details>

<details>
<summary>Normalization uncertainties</summary>
<br>

For the moment, let’s just look at our symmetric uncertainties and later we will look at a shape uncertainty.

* **Luminosity uncertainties:** As we saw in the previous example, the luminosity is a symmetric uncertainty. Each year, the amount of data collected (“luminosity”) is measured. This measurement has some uncertainty which means the uncertainty changes for each year and is uncorrelated between the years.

* **Cross section uncertainties:** The cross section uncertainty is another symmetric uncertainty added to each Monte Carlo sample. We have applied fairly generous uncertainties here because we are in a high-$p_T$ regime where dedicated measurements of these processes' (e.g. top pair production, single top production) cross sections have not been measured.

**Question:** Should each Monte Carlo sample have a different luminosity uncertainty?

**Question:** Should the signal Monte Carlo sample have a cross section uncertainty?

<details>
<summary>Solution</summary>
<br> 

No. This is because the “cross section” of the signal is exactly what we want to fit for (ie. the parameter of interest/POI/r/mu)! One might ask then why we assign an uncertainty from the luminosity to the signal when the luminosity is just another normalization uncertainty. The answer is that the luminosity uncertainty is fundamentally different from the cross section uncertainties because it is correlated across all of the processes - if it goes up for one, it goes up for all. The cross section uncertainties though are per-process - the ttbar cross section can go up and the single top contribution will remain the same.

</details>

</details>

**Exercise:** Run `python bstar.py` to create the 2DAlphabet fitting workspace without adding any new systematics. This will produce a directory `tWfits/`. 

**Exercise:** Now go to `bstar.py` and go to the main loop. Comment out the line `make_workspace()` so that the workspace isn't re-made, and un-comment the line `ML_fit(sig,verbosity=1)`. You can increase the verbosity to `2` if you want more information on what's happening with the minimizer beneath the hood. Run the maximum likelihood fit with `python bstar.py`.

The fit results will now be stored under `tWfits/tW-2400_area/`. 

<details>
<summary>The Combine card</summary>
<br>

2DAlphabet will parse the information stored in the JSON config file and automatically generate a Combine card. Check out what 2DAlphabet generated for this fit by reading the card stored at `tWfits/tW-2400_area/card.txt`. For more information on how to read and understand the card, check the official [Combine documentation](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/latest/part2/settinguptheanalysis/).

</details>

<details>
<summary>Shape uncertainties</summary>
<br>

With just the luminosity and cross section uncertainties, we have accounted for some normalization uncertainties. There are certainly uncertainties which affect the shapes of distributions (our simulation is not perfect in many ways). These can be accounted for via vertical template interpolation (also called “template morphing”). The high-level explanation is that you can provide an “up” shape and a “down” shape in addition to the “nominal” and the algorithm will map “up” to +1 sigma and “down” to -1 sigma on a Gaussian constraint. Combine will then automatically interpolate between these different shapes to obtain a continuous parameterization of the shape uncertainty. The first of these “shape” uncertainties is the "Top pT" uncertainty.

* **Top pT uncertainties:** The TOP group has recommendations for how to account for consistent but not-understood disagreements in the top pT spectrum between ttbar simulation and ttbar in data (see this twiki for more information). This analysis falls into Case 3.1 from that twiki. The short version of the recommendation is that one should measure the top pT spectrum in a dedicated ttbar control region which is exactly what the ttbar measurement region of the analysis is for.

For this analysis the top jet mass window is 105-220 GeV which constrains the ttbar background but does not leave a lot of room to work with when blinded. Fortunately, mtt is correlated with top pT so we can just measure the correction in the 2D space while also fitting for QCD and all of the other systematics in the ttbar measurement region and signal region!

The decision of the correction for the ttbar top pT spectrum was pretty simple. We just took the functional form of the top pT correction (for POWHEG+Pythia8) farther down and rewrote it as $e^{0.0615 \alpha - 0.0005 \beta p_T}$ where $\alpha$ and $\beta$ are nominally 1 (so just the regular correction on that page). Then each $ \alpha $ and $ \beta $ are individually varied to 0.5 and 1.5 to be our uncertainty templates for the vertical interpolation. These are what are assigned to Tpt-alpha and Tpt-beta. For example, if the post-fit value of Tpt-alpha was 1 and Tpt-beta was -1, the final effective form of our Tpt reweighting function would be $e^{0.0615 * 1.50 - 0.0005 * 0.50 * pT}$.

</details>

**Exercise:**  Add the Top $p_T$ uncertainties to the appropriate processes and see the how the fits, Combine card, and statistics change. The uncertainties are already included in the JSON config file section called "SYSTEMATICS". However, they have not been added to the individual processes. To start this exercise, add the `Tpt-alpha` and `Tpt-beta` uncertainties to the appropriate processes. Which processes should these uncertainties be added to? 

* Change the JSON file to add the top-pT shape uncertainties to the appropriate processes
* Save the existing fit directory by renaming it, e.g. `mv tWfits/ tWfits_only_norm_uncerts/`
* Re-make the 2DAlphabet workspace and re-run the fit by uncommenting the `make_workspace()` line in `bstar.py`. This will ensure that the changes to the JSON are propagated to the Combine card. 
* Compare the combine cards after re-running the fits using `diff` to see the changes before and after adding in the top pT uncertainty. E.g. `diff tWfits_only_norm_uncerts/tW-2400_area/card.txt tWfits/tW-2400_area/card.txt`

<details>
<summary>Solution</summary>
<br>

The Top pT uncertainties should be applied to the ttbar and ttbar-semilep processes. For example, for 2016 $t\bar{t}$ samples:

```
        "16_ttbar": {
            "SYSTEMATICS": ["lumi","ttbar_xsec","Tpt-alpha","Tpt-beta"],
            "SCALE": 1.0,
            "COLOR": 2,
            "TYPE": "BKG",
            "TITLE": "t#bar{t}",
            "LOC": "path/FILE:HIST"
        },
```

Look under `SYSTEMATICS` to remind yourself how the `Tpt-alpha/beta` systematics are defined.

</details>

**Exercise:** Compare the results of the ML fit with and without applying these shape uncertainties by looking at the following files in `tWfits/tW-2400_area/`:

* `fitDiagnosticsTest.root` (this file stores the results from the ML fit)
* `FitDiagnostics.log` (the `stdout` output from Combine, which prints some statistics)

**Exercise:** Take a look at the shape templates that have been produced for the ttbar processes. They will be stored under `tWfits/UncertPlots/`. 

**Exercise:** Run `ls /eos/user/c/cmsdas/2023/long-ex-b2g/rootfiles` and `grep` for the various processes (`ttbar`, `ttbar-semilep`, `singletop_tW`, `signalLH*`) to see which systematic variation files they have. Open up the `default` file for each and see which systematic variation histograms they have. Add one of each type to the JSON and re-run the fit. 

<details>
<summary>Solution</summary>
<br>

E.g., for `signalLH2400` we can add uncertainty in the jet energy corrections, which affect the scale and resolution of the measured jet energy.

```
(twoD-env) Singularity> ls /eos/user/c/cmsdas/2023/long-ex-b2g/rootfiles | grep signalLH2400
TWpreselection16_signalLH2400_tau32medium_JER_down_default.root
TWpreselection16_signalLH2400_tau32medium_JER_down_sideband.root
TWpreselection16_signalLH2400_tau32medium_JER_down_ttbar.root
TWpreselection16_signalLH2400_tau32medium_JER_up_default.root
TWpreselection16_signalLH2400_tau32medium_JER_up_sideband.root
TWpreselection16_signalLH2400_tau32medium_JER_up_ttbar.root
TWpreselection16_signalLH2400_tau32medium_JES_down_default.root
TWpreselection16_signalLH2400_tau32medium_JES_down_sideband.root
TWpreselection16_signalLH2400_tau32medium_JES_down_ttbar.root
TWpreselection16_signalLH2400_tau32medium_JES_up_default.root
TWpreselection16_signalLH2400_tau32medium_JES_up_sideband.root
TWpreselection16_signalLH2400_tau32medium_JES_up_ttbar.root
TWpreselection16_signalLH2400_tau32medium_JMR_down_default.root
TWpreselection16_signalLH2400_tau32medium_JMR_down_sideband.root
TWpreselection16_signalLH2400_tau32medium_JMR_down_ttbar.root
TWpreselection16_signalLH2400_tau32medium_JMR_up_default.root
TWpreselection16_signalLH2400_tau32medium_JMR_up_sideband.root
TWpreselection16_signalLH2400_tau32medium_JMR_up_ttbar.root
TWpreselection16_signalLH2400_tau32medium_JMS_down_default.root
TWpreselection16_signalLH2400_tau32medium_JMS_down_sideband.root
TWpreselection16_signalLH2400_tau32medium_JMS_down_ttbar.root
TWpreselection16_signalLH2400_tau32medium_JMS_up_default.root
TWpreselection16_signalLH2400_tau32medium_JMS_up_sideband.root
TWpreselection16_signalLH2400_tau32medium_JMS_up_ttbar.root
TWpreselection16_signalLH2400_tau32medium_default.root
```

These are an example of uncertainties stored *in different files* for a given process.

If we open the `default` file we can check what sorts of uncertainties are stored *in the same file* for the given process:

```
(twoD-env) Singularity> root -l /eos/user/c/cmsdas/2023/long-ex-b2g/rootfiles/TWpreselection16_signalLH2400_tau32medium_default.root
root [0]
Attaching file /eos/user/c/cmsdas/2023/long-ex-b2g/rootfiles/TWpreselection16_signalLH2400_tau32medium_default.root as _file0...
(TFile *) 0x4e44ab0
root [1] .ls
TFile**         /eos/user/c/cmsdas/2023/long-ex-b2g/rootfiles/TWpreselection16_signalLH2400_tau32medium_default.root
 TFile*         /eos/user/c/cmsdas/2023/long-ex-b2g/rootfiles/TWpreselection16_signalLH2400_tau32medium_default.root
  KEY: TH2F     MtwvMtPass;1    mass of tw vs mass of top - Pass
  KEY: TH2F     MtwvMtPassNotrig;1      mass of tw vs mass of top - Pass - No trigger
  KEY: TH2F     MtwvMtFail;1    mass of tw vs mass of top - Fail
  KEY: TH2F     MtwvMtFailSub;1 mass of tw vs mass of top - FailSub
  KEY: TH1F     nev;1   nev
  KEY: TH1F     Mw;1    m_{W} (GeV)
  KEY: TH1F     sigDecay;1      sigDecay
  KEY: TH1F     wmatchCount;1   W match count
  KEY: TH2F     MtwvMtPassPDFup;1       mass of tw vs mass of top PDF up - Pass
  KEY: TH2F     MtwvMtPassPDFdown;1     mass of tw vs mass of top PDF down - Pass
  KEY: TH2F     MtwvMtPassPUup;1        mass of tw vs mass of top PU up - Pass
  KEY: TH2F     MtwvMtPassPUdown;1      mass of tw vs mass of top PU down - Pass
  KEY: TH2F     MtwvMtPassTop3up;1      mass of tw vs mass of top sf up (merged) - Pass
  KEY: TH2F     MtwvMtPassTop3down;1    mass of tw vs mass of top sf down (merged) - Pass
  KEY: TH2F     MtwvMtPassTop2up;1      mass of tw vs mass of top sf up (semi-merged) - Pass
  KEY: TH2F     MtwvMtPassTop2down;1    mass of tw vs mass of top sf down (semi-merged) - Pass
  KEY: TH2F     MtwvMtPassTop1up;1      mass of tw vs mass of top sf up (not merged) - Pass
  KEY: TH2F     MtwvMtPassTop1down;1    mass of tw vs mass of top sf down (not merged) - Pass
  KEY: TH2F     MtwvMtPassScaleup;1     mass of tw vs mass of Q^2 scale up - Pass
  KEY: TH2F     MtwvMtPassScaledown;1   mass of tw vs mass of Q^2 scale down - Pass
  KEY: TH2F     MtwvMtPassSjbtagup;1    mass of tw vs mass of sjbtag sf up - Pass
  KEY: TH2F     MtwvMtPassSjbtagdown;1  mass of tw vs mass of sjbtag sf down - Pass
  KEY: TH2F     MtwvMtPassTrigup;1      mass of tw vs mass of top trig up - Pass
  KEY: TH2F     MtwvMtPassTrigdown;1    mass of tw vs mass of top trig down - Pass
  KEY: TH2F     MtwvMtPassPrefireup;1   mass of tw vs mass of top prefire up - Pass
  KEY: TH2F     MtwvMtPassPrefiredown;1 mass of tw vs mass of top prefire down - Pass
...
```

We can see that, among others, uncertainty templates have been generated corresponding to $\pm 1\sigma$ (`up/down`) variations in the pileup (`PU`) reweighting. Let's add both. To do so, add the following to the `SYSTEMATICS` section of `bstar.json`:

```
        "Pileup": {
            "ALIAS": "PU",
            "CODE": 2,
            "UP": "path/FILE:HIST_UP",
            "DOWN": "path/FILE:HIST_DOWN",
            "SCALE": 1.0,
            "SIGMA": 1.0
        },
	"JetEnergyResolution": {
            "ALIAS": "JER",
            "CODE": 3,
            "UP": "path/FILE_UP:HIST",
            "DOWN": "path/FILE_DOWN:HIST",
            "SCALE": 1.0,
            "SIGMA": 1.0
        }
```

**NOTE:** Pay close attention to the `CODE` and `UP/DOWN` definitions describing where the variations are stored (in different files, or different histograms).

Now, add the systematics to the process (we've chosen `signalLH2400`), so:

```
        "16_SIGNAME": {
            "SYSTEMATICS":["lumi","Pileup","JetEnergyResolution"],
            "SCALE": 1.0,
            "COLOR": 1,
            "TYPE": "SIGNAL",
            "TITLE": "b*_{LH} (2.4 TeV)",
            "LOC": "path/FILE:HIST"
        },
	... # do the same for the other years (17/18_SIGNAME)
```

You should now be ready to re-run `bstar.py` (remembering to remake the workspace so that the changes to the JSON propagate).

</details>

**Exercise (time-permitting):** Add the following systematics to all the relevant processes. Doing so will provide the fit with a more accurate description of the systematic uncertainties to which the Monte Carlo Note that **all** files for a given process must have the systematic histogram or else 2DAlphabet will fail:

* Jet energy and mass correction uncertainties:
   * `JER/JES/JMR/JMS`
   * These uncertainties are found in different *files*
* Experimental uncertainties:
   * Parton distribution functions: `PDF`
   * Pileup: `PU`
   * Trigger efficiency: `Trig`
   * L1 Prefiring: `Prefire`
   * These uncertainties are found in different *histograms* within the same file


**Exercise:** Now plot the results by commenting `make_workspace()`, `ML_fit(sig,verbosity=1)` and then un-commenting `plot_fit(sig)`, then re-running `python bstar.py`. Investigate the following files: 

* `tWfits/tW-2400_area/nuisance_pulls.pdf` - this shows a comparison b/w the pre- and post-fit values of the nuisance parameters. More information [here](http://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part3/nonstandard/#pre-and-post-fit-nuisance-parameters)
* `tWfits/tW-2400_area/rpf_params_QCD_rratio_fitb.txt` and `tWfits/tW-2400_area/rpf_params_QCD_rratio_fits.txt` - these are text files showing the post-fit values of the transfer function for the background-only and signal-plus-background hypotheses, respectively.
* `tWfits/tW-2400_area/plots_fit_b/correlation_matrix.pdf` - correlation between the nuisance parameters in the fit
* `tWfits/tW-2400_area/plots_fit_b/postfit_projx.pdf` and `tWfits/tW-2400_area/plots_fit_b/postfit_projy.pdf` - the results of the fit projected onto the x (resonance mass) and y (top mass) axes, respectively. These plots show the pulls, i.e. the difference between the data and prediction, divided by the statistical and systematic uncertainty added in quadrature, in the lower panes. Note that in the background-only fit plots, the pre-fit signal distributions are overlaid to provide an idea of the theoretical signal distribution. 

**Exercise:** Check the nuisance parameter impacts by commenting `plot_fit(sig)` and un-commenting `calculate_impacts(sig)` in `bstar.py`. Re-run the script, read the Combine output on the terminal, and then take a look at the following files:

* `tWfits/tW-2400_area/higgsCombine_paramFit_Test_*.root` - these are the ROOT files generated by Combine containing the results of the nuisance parameter scans along the POI
* `tWfits/tW-2400_area/impacts.pdf` - a plot summarizing the nuisance parameter values and impacts

More information about nusiance parameter impacts can be found [here](http://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part3/nonstandard/#nuisance-parameter-impacts).

**Exercise:** Now let's try changing the parameterization of the pass-fail ratio. 

To do so, let's alter the `bstar.py` script so that it creates a new working directory for fits using this pass-fail ratio parameterization:

* Change the parameterization in line 120 from `2x1` to `0x0` (a polynomial constant in both mt and mtw): `@0`
* Then ensure that a new working directory is created by opening `bstar.py` in Vim (`vi` in the container):
   * First press `Esc` to enter "normal mode"
   * Then type `:` to enter "command-line-mode", a colon (`:`) will appear at the bottom of the screen
   * You can now type the following to find and replace all occurrences of the original working directory with a new name:
    * `%s/tWfits/tWfits_0x0/g`
   * Exit Vim using `:wq` to write and exit (`:q` will exit normally, `:q!` will quit without saving changes)
* Re-run the fit and check out the differences afterwards.

You can repeat the above steps for different transfer function parameterizations, listed here:

* "1x0": `'@0+@1*x'`
* "0x1": `'@0+@1*y'`
* "1x1": `'(@0+@1*x)*(1+@2*y)'`
* "1x2": `'(@0+@1*x)*(1+@2*y+@3*y*y)'`
* "2x1": `'(@0+@1*x+@2*x**2)*(1+@3*y)'`
* "2x2": `'(@0+@1*x+@2*x**2)*(1+@3*y+@4*y**2)'`
* "2x3": `'(@0+@1*x+@2*x*x)*(1+@3*y+@4*y*y+@5*y*y*y)'`

and so on. Try to make a different working directory each time. You can even try a different function (product of exponentials, e.g.) to parameterize the transfer function. **Note:** Not all transfer function parameterizations will result in a stable fit. In fact, you might reach a point where adding terms to the polynomial will reach a point of diminishing returns in terms of the goodness of fit.

**Exercise:** After trying a few of the transfer function parameterizations, perform an F-Test by running `python FTest.py`

* **TO-DO:** update documentation here

**Exercise:** Finally, obtain a goodness-of-fit metric for your fit with all of these included systematics. To do so, comment out `make_workspace()`, `ML_fit(sig, verbosity=1)`, `plot_fit(sig)`, and `calculate_impacts(sig)`. Then un-comment `GoF(sig, tf='', nToys=10, condor=False)` and re-run `python bstar.py`. For now, run the jobs locally by keeping `condor=False`, but feel free to generate as many toys as you want with `nToys`. More toys will take longer to run, but provide a better distribution of the test statistic. 

* **TO-DO:** update documentation here

</details>

# Exercise 7: Limit setting

<details>
<summary>How to obtain limits</summary>
<br>

The limit setting program for 2DAlphabet needs to be run for each mass point under consideration and for each year. By default, `bstar.py` is set up to run the limit setting code for each mass point. The fitting routine (`ML_fit()`) must be run for each mass point before the limit routine (`perform_limit(sig)`) can be run.

**Exercise:** First run the limit routiune (`perform_limit(sig)`) for each of the mass points. There are 19 mass points between 1400 and 4000 (1400, 1600, …), so it might be a good idea for each of you to choose 4 mass points to do and then combine the results together.

* First, add all of the signals to the `SIGNAME` key in the JSON config file under `GLOBAL`.
* Then, in the main loop in `bstar.py`, add the signal masses to the `for` loop.
* Use `make_workspace()` to create the workspace with all of the signal files added internally to the 2DAlphabet workspace
* Comment out `make_workspace()` and un-comment `ML_fit(sig,verbosity=1)` and `perform_limit(sig)`. There is no need to plot the fits or run GoFs for the sake of this exercise. 
* You will now have an output ROOT file for each signal mass point from which the expected and observed limits can be obtained. 
   * **NOTE:** The files will all be stored under `tWfits/` directory, with subdirectories for each mass point. E.g. `tWfits/tW-2400_area`, `tWfits/tW-2600_area`, `tWfits/tW-2800_area`, etc...

**Exercise:** You are now ready to plot the expected and observed limits. To do so, run the following script:

```
python set_limit.py -s test_signals.txt [--unblind]
```

The optional `--unblind` flag will unblind the data and show the observed limit in the plot. The `test_signals.txt` file uses commas and newlines to separate the information. It looks like this:

```
LimitLH1400,  LimitLH1600,  LimitLH1800
1400,         1600,         1800
0.7848,       0.3431,       0.1588
1.0,          0.1,          0.1
```

The rows represent, in order:

* Folder location where `higgsCombineTest.AsymptoticLimits.mH120.root` lives (this was generated by `perform_limit(sig)` in `bstar.py`)
* Signal mass, in GeV
* Theoretical cross section, for plotting the theory curve
* Normalized cross section used when generating the scaled input histograms.
   * These values are known to the analyzers who generated the 2DAlphabet input histograms, but not to you. For the rest of the mass points, use these values:
    * mass 1400-2000: 1.0
    * mass 2200-3000: 0.1
    * mass >3000: 0.01
* The values for both theory and input cross section can also be found in `bstar_signalsLH_unblind.txt`

Now use these steps to run the limit setting program on all 19 mass points. 

Did you discover new physics? How do you know?

</details>
